/******************************************************************************
*
* libkuso
* List-likes support code
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "kuso.h"

struct kuso_pvec_t {
  size_t cnt;
  size_t size;
  void** data;
};

/* Pointer Vector stuff */
static inline void _kuso_pvec_alloc(kuso_pvec_t* vec)
{
  if(vec->data) {
    vec->data = mem_realloc_cnt_type(vec->data, vec->size+1, void*);
  }else{
    vec->data = mem_zalloc_cnt_type(vec->size+1, void*);
  }
}

static inline void _kuso_pvec_realloc(kuso_pvec_t* vec, size_t tgtcnt)
{
  while(tgtcnt >= vec->size) {
    if(vec->size == 0)
      vec->size++;
    else
      vec->size *= 2;
    _kuso_pvec_alloc(vec);
  }

  vec->cnt = tgtcnt;
}

kuso_pvec_t* kuso_pvec_new(size_t num)
{
  kuso_pvec_t* vec;
  vec = mem_alloc_type(kuso_pvec_t);
  vec->cnt = 0;
  vec->size = num;
  vec->data = NULL;
  _kuso_pvec_alloc(vec);

  return vec;
}

void kuso_pvec_delete(kuso_pvec_t* vec)
{
  if(vec->data)
    mem_free(vec->data);
  mem_free(vec);
}

size_t kuso_pvec_size(kuso_pvec_t* vec)
{
  return vec->cnt;
}

void* kuso_pvec_get(kuso_pvec_t* vec, size_t idx)
{
  if(idx >= vec->cnt)
    return NULL;
  return vec->data[idx];
}

void** kuso_pvec_iter_void(kuso_pvec_t* vec)
{
  if(vec->data != NULL)
    vec->data[vec->cnt] = NULL;
  return vec->data;
}

void kuso_pvec_expand(kuso_pvec_t* vec, size_t cnt)
{
  if(cnt <= vec->cnt)
    return;

  _kuso_pvec_realloc(vec, cnt);
}

void kuso_pvec_append(kuso_pvec_t* vec, void* elem)
{
  size_t pos = vec->cnt;
  _kuso_pvec_realloc(vec, vec->cnt+1);
  vec->data[pos] = elem;
}

void kuso_pvec_concat(kuso_pvec_t* tgt, kuso_pvec_t* src)
{
  size_t ncnt = tgt->cnt + src->cnt;
  size_t pos = tgt->cnt;
  _kuso_pvec_realloc(tgt, ncnt);
  memcpy(tgt->data + pos, src->data, src->cnt * sizeof(void*));
}

void kuso_pvec_insert(kuso_pvec_t* vec, size_t idx, void* elem)
{
  size_t pos = vec->cnt;
  if(idx > vec->cnt)
    idx = vec->cnt;

  _kuso_pvec_realloc(vec, vec->cnt+1);

  if(idx < pos)
    memmove(vec->data + idx + 1, vec->data + idx, (pos - idx) * sizeof(void*));

  vec->data[idx] = elem;
}

void kuso_pvec_prepend(kuso_pvec_t* vec, void* elem)
{
  kuso_pvec_insert(vec, 0, elem);
}

void* kuso_pvec_remove(kuso_pvec_t* vec, size_t idx)
{
  void* dat;
  if(idx >= vec->cnt)
    return NULL;

  dat = vec->data[idx];
  vec->cnt--;
  if(vec->cnt > idx)
    memmove(vec->data + idx, vec->data + idx + 1, (vec->cnt - idx + 1) * sizeof(void*));

  return dat;
}

void* kuso_pvec_pop(kuso_pvec_t* vec)
{
  return kuso_pvec_remove(vec, kuso_pvec_size(vec)-1);
}


#define TYPE_PREFIX p
#define TYPE_TYPE void*
#define TYPE_EMPTY NULL
#include "kuso_list_pi.i"

#define TYPE_PREFIX
#define TYPE_TYPE uintmax_t
#define TYPE_EMPTY 0
#include "kuso_list_pi.i"
