/******************************************************************************
*
* libkuso
* Tokenizer support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef LIBKUSO_TKN_H_
#define LIBKUSO_TKN_H_

typedef struct kuso_tkn_t kuso_tkn_t;

struct kuso_tkn_t {
  kuso_tkn_t* next;
  kuso_tkn_t* prev;

  char *text;
  size_t len;

  int last;
  int cnt;
};

size_t kuso_tkncpy(char* dst, char* str, char* split, char* nocopy, size_t* sz, int* last);
kuso_tkn_t* kuso_nexttkn(char** str, char* split, char* nocopy, int allow_empty);
kuso_tkn_t* kuso_tokenize(char* str, char* split, char* nocopy);
void kuso_tknfree(kuso_tkn_t* tkn);
void kuso_tknfree_nexts(kuso_tkn_t* tkn);
void kuso_tknfree_prevs(kuso_tkn_t* tkn);
void kuso_tknfree_all(kuso_tkn_t* tkn);
/* FALSE = not same, TRUE = same */
int kuso_cmptkn(kuso_tkn_t* tkn, char* str);
kuso_tkn_t* kuso_tknlast(kuso_tkn_t* tkn);

char* kuso_nextline(char* str);
char* kuso_stripcomment(char* str, char* comments);
char* kuso_stripspace(char* str);

#endif
