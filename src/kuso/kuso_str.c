/******************************************************************************
*
* libkuso
* String support code
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "kuso.h"

int kuso_vasprintf(char** out, char* str, va_list ap)
{
  int ret;
  char* tmp;
  va_list ap2;
  va_copy(ap2, ap);

  ret = vsnprintf(NULL, 0, str, ap);

  tmp = mem_alloc(ret + 1);
  if(tmp == NULL) {
    ret = -1;
    goto _fail;
  }

  ret = vsprintf(tmp, str, ap2);

  tmp[ret] = '\0';
  if(*out != NULL)
    mem_free(*out);
  *out = tmp;

_fail:
  va_end(ap2);
  return ret;
}

int kuso_asprintf(char** out, char* str, ...)
{
  int ret;
  va_list ap;
  va_start(ap, str);
  ret = kuso_vasprintf(out, str, ap);
  va_end(ap);
  return ret;
}

char* kuso_strcat(char** dst, char* src)
{
  char* nstr;
  int dlen, slen;

  if(*dst == NULL && src == NULL)
    return NULL;

  if(*dst == NULL) {
    *dst = mem_strdup(src);
    if(*dst == NULL)
      return NULL;
    return *dst;
  }

  if(src == NULL)
    return *dst;

  dlen = strlen(*dst);
  slen = strlen(src);
  nstr = mem_alloc(dlen + slen + 1);
  if(nstr == NULL) {
    return NULL;
  }

  strncpy(nstr, *dst, dlen);
  strncpy(nstr+dlen, src, slen);
  nstr[dlen+slen] = '\0';

  mem_free(*dst);
  *dst = nstr;

  return *dst;
}

char* kuso_vcatsprintf(char** dst, char* str, va_list ap)
{
  char* tmp;
  int ret;

  tmp = NULL;
  ret = kuso_vasprintf(&tmp, str, ap);
  if(ret == -1)
    return NULL;
  if(*dst == NULL) {
    *dst = tmp;
    return *dst;
  }
  kuso_strcat(dst, tmp);
  mem_free(tmp);
  return *dst;
}

char* kuso_catsprintf(char** out, char* str, ...)
{
  char* ret;
  va_list ap;
  va_start(ap, str);
  ret = kuso_vcatsprintf(out, str, ap);
  va_end(ap);
  return ret;
}

int xtoi_full(char* str, int base, char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  char* ostr;

  ostr = str;
  if(base == -1) base = 10;

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val;
}

int xtoi_bs(char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(char* str) { return xtoi_full(str, -1, NULL); }
