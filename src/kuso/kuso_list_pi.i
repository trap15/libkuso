/******************************************************************************
*
* libkuso
* List-likes support code (for stuff with both pointer/integer types)
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#define kuso_xfifo_t CPP_CONCAT3(kuso_,TYPE_PREFIX,fifo_t)
#define kuso_xstack_t CPP_CONCAT3(kuso_,TYPE_PREFIX,stack_t)
#define kuso_fifo_fun(n) CPP_CONCAT4(kuso_,TYPE_PREFIX,fifo_,n)
#define kuso_stack_fun(n) CPP_CONCAT4(kuso_,TYPE_PREFIX,stack_,n)

struct kuso_xfifo_t {
  size_t size;
  size_t cnt;
  size_t w_ptr;
  size_t r_ptr;
  TYPE_TYPE* data;
};

struct kuso_xstack_t {
  size_t size;
  size_t cnt;
  TYPE_TYPE* data;
};

/* FIFO stuff */
kuso_xfifo_t* kuso_fifo_fun(new)(size_t size)
{
  kuso_xfifo_t* fifo;
  fifo = mem_alloc_type(kuso_xfifo_t);
  fifo->size = size;
  fifo->cnt = 0;
  fifo->r_ptr = 0;
  fifo->w_ptr = 0;
  fifo->data = mem_alloc_cnt_type(size, TYPE_TYPE);

  return fifo;
}

void kuso_fifo_fun(delete)(kuso_xfifo_t* fifo)
{
  mem_free(fifo->data);
  mem_free(fifo);
}

void kuso_fifo_fun(push)(kuso_xfifo_t* fifo, TYPE_TYPE data)
{
  fifo->cnt++;
  fifo->data[fifo->w_ptr++] = data;
  fifo->w_ptr %= fifo->size;
}

TYPE_TYPE kuso_fifo_fun(pull)(kuso_xfifo_t* fifo)
{
  TYPE_TYPE data;
  if(fifo->cnt <= 0) return TYPE_EMPTY;

  fifo->cnt--;
  data = fifo->data[fifo->r_ptr++];
  fifo->r_ptr %= fifo->size;
  return data;
}

TYPE_TYPE kuso_fifo_fun(peek)(kuso_xfifo_t* fifo)
{
  if(fifo->cnt <= 0) return TYPE_EMPTY;
  return fifo->data[fifo->r_ptr];
}

int kuso_fifo_fun(empty)(kuso_xfifo_t* fifo)
{
  if(fifo->cnt <= 0)
    return TRUE;
  else
    return FALSE;
}

int kuso_fifo_fun(has_data)(kuso_xfifo_t* fifo)
{
  return !kuso_fifo_fun(empty)(fifo);
}

size_t kuso_fifo_fun(count)(kuso_xfifo_t* fifo)
{
  return fifo->cnt;
}


/* Stack stuff */
static void kuso_stack_fun(alloc)(kuso_xstack_t* stk)
{
  if(stk->data) {
    stk->data = mem_realloc_cnt_type(stk->data, stk->size, TYPE_TYPE);
  }else{
    stk->data = mem_alloc_cnt_type(stk->size, TYPE_TYPE);
  }
}

static void kuso_stack_fun(realloc)(kuso_xstack_t* stk)
{
  if(stk->size == 0) {
    stk->size = 1;
  }else{
    stk->size++;
  }
  kuso_stack_fun(alloc)(stk);
}

kuso_xstack_t* kuso_stack_fun(new)(size_t num)
{
  kuso_xstack_t* stk;
  stk = mem_alloc_type(kuso_xstack_t);
  stk->cnt = 0;
  stk->size = num;
  stk->data = NULL;
  if(stk->size)
    kuso_stack_fun(alloc)(stk);

  return stk;
}

void kuso_stack_fun(delete)(kuso_xstack_t* stk)
{
  if(stk->data)
    mem_free(stk->data);
  mem_free(stk);
}

void kuso_stack_fun(push)(kuso_xstack_t* stk, TYPE_TYPE data)
{
  if(stk->cnt >= stk->size)
    kuso_stack_fun(realloc)(stk);

  stk->data[stk->cnt++] = data;
}

TYPE_TYPE kuso_stack_fun(pop)(kuso_xstack_t* stk)
{
  TYPE_TYPE data;
  if(stk->cnt <= 0) return TYPE_EMPTY;

  data = stk->data[--stk->cnt];
  return data;
}

TYPE_TYPE kuso_stack_fun(peek)(kuso_xstack_t* stk)
{
  if(stk->cnt <= 0) return TYPE_EMPTY;
  return stk->data[stk->cnt-1];
}

TYPE_TYPE kuso_stack_fun(get)(kuso_xstack_t* stk, ssize_t idx)
{
  size_t ptr = idx;

  if(idx < 0) ptr = stk->cnt + idx;

  if(ptr >= stk->cnt)
    return TYPE_EMPTY;
  return stk->data[ptr];
}

int kuso_stack_fun(empty)(kuso_xstack_t* stk)
{
  if(stk->cnt <= 0)
    return TRUE;
  else
    return FALSE;
}

int kuso_stack_fun(has_data)(kuso_xstack_t* stk)
{
  return !kuso_stack_fun(empty)(stk);
}

size_t kuso_stack_fun(count)(kuso_xstack_t* stk)
{
  return stk->cnt;
}


#undef TYPE_PREFIX
#undef TYPE_TYPE
#undef TYPE_EMPTY
