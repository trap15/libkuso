/******************************************************************************
*
* libkuso
* Memory tracking
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef LIBKUSO_MEM_TRACK_H_
#define LIBKUSO_MEM_TRACK_H_

void* _memtrk_alloc(size_t size, int line, char* file);
void* _memtrk_alloc_cnt(size_t num, size_t size, int line, char* file);
void* _memtrk_zalloc(size_t size, int line, char* file);
void* _memtrk_zalloc_cnt(size_t num, size_t size, int line, char* file);
void* _memtrk_realloc(void* ptr, size_t size, int line, char* file);
void* _memtrk_realloc_cnt(void* ptr, size_t num, size_t size, int line, char* file);
char* _memtrk_strdup(char* str, int line, char* file);
void _memtrk_free(void* ptr, int line, char* file);
void _memtrk_print_leaks(void);
int _memtrk_leaked(void);

#undef mem_alloc
#undef mem_alloc_cnt
#undef mem_zalloc
#undef mem_zalloc_cnt
#undef mem_realloc
#undef mem_realloc_cnt
#undef mem_free
#undef mem_strdup

#define mem_alloc(x)           _memtrk_alloc(x,__LINE__,__FILE__)
#define mem_alloc_cnt(x,y)     _memtrk_alloc_cnt(x,y,__LINE__,__FILE__)
#define mem_zalloc(x)          _memtrk_zalloc(x,__LINE__,__FILE__)
#define mem_zalloc_cnt(x,y)    _memtrk_zalloc_cnt(x,y,__LINE__,__FILE__)
#define mem_realloc(x,y)       _memtrk_realloc(x,y,__LINE__,__FILE__)
#define mem_realloc_cnt(x,y,z) _memtrk_realloc_cnt(x,y,z,__LINE__,__FILE__)
#define mem_free(x)            _memtrk_free(x,__LINE__,__FILE__)
#define mem_strdup(x)          _memtrk_strdup(x,__LINE__,__FILE__)

#endif
