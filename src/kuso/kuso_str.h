/******************************************************************************
*
* libkuso
* String support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef LIBKUSO_STR_H_
#define LIBKUSO_STR_H_

/* Versatile xtoi functions.
 * xtoi_full allows specifying the initial base, and gets a pointer for the
 * end of the numeric part of the string.
 * xtoi_bs ignores the end of the string.
 * xtoi specifies the default base as 10.
 * Passing -1 to base uses the default base.
 * Passing NULL to sstr is allowed.
 */
int xtoi_full(char* str, int base, char** sstr);
int xtoi_bs(char* str, int base);
int xtoi(char* str);

/* Clone of asprintf (to remove GNU requirement) with a minor change:
 *   Using *out in the arguments is allowed.
 *   If *out was not NULL, it is freed before returning.
 *     Thus you must initialize *out before calling this.
 */
int kuso_asprintf(char** out, char* str, ...);
/* kuso_asprintf but takes a va_list */
int kuso_vasprintf(char** out, char* str, va_list ap);

/* Clone of strcat with some key differences:
 *   src may overlap *dst.
 *   src may be NULL (but why would you?).
 *   *dst does not need to be big enough to hold both strings.
 *   *dst may be NULL.
 *   The pointer at dst may be changed.
 *   Automatically frees the old *dst if needed.
 */
char* kuso_strcat(char** dst, char* src);

/* Concatenated a formatted string to another string.
 * If *dst is NULL, it acts like kuso_asprintf.
 */
char* kuso_catsprintf(char** dst, char* str, ...);
/* kuso_catsprintf but takes a va_list */
char* kuso_vcatsprintf(char** out, char* str, va_list ap);

#endif
